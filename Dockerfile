FROM golang:1.17

RUN apt-get update 
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt install nodejs -y
# RUN apt install npm -y
RUN npm install --global yarn

RUN apt install -y wget unzip supervisor
# # setting mexico timezone on container
# ENV TZ=America/Mexico_City
# RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /grafana

# ADD ./grafana/package.json .
COPY ./grafana .
RUN yarn install --pure-lockfile



ADD ./supervisord.conf /etc/

ENTRYPOINT ["supervisord", "--nodaemon", "--configuration", "/etc/supervisord.conf"]
